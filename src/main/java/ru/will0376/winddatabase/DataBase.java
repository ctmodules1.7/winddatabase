package ru.will0376.winddatabase;


import ru.will0376.windevents.utils.Logger;

import java.sql.*;
import java.util.ArrayList;

public class DataBase {
	static Config cfg = WindDB.config;

	public static Connection connect() {
		Connection dbConnection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Logger.log(3, "[DataBase.connect]", "Driver found.");
			dbConnection = DriverManager.getConnection(cfg.getHost(), cfg.getUser(), cfg.getPass());
			Logger.log(3, "[DataBase.connect]", "Connection successfully");
			return dbConnection;
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void checkTables() {
		try {
			Connection c = connect();
			c.prepareStatement("CREATE TABLE IF NOT EXISTS %logtable%(id int auto_increment primary key, %url% text, %player% text, %admin% text, %time% text)"
					.replace("%logtable%", cfg.getTableName())
					.replace("%url%", cfg.getColumnURL())
					.replace("%player%", cfg.getColumnPlayer())
					.replace("%admin%", cfg.getColumnAdmin())
					.replace("%time%", cfg.getColumnTime())
			).execute();
		} catch (Exception e) {
			Logger.log(1, "[DataBase.CheckTables]", e.getMessage());
		}
	}

	public static void addToLog(String nick, String admin, String url, String time) {
		try {
			Logger.printWithName(3, "[DataBase.addToLog]", nick + " " + admin + " " + url + " " + time);
			connect().prepareStatement(("Insert into %logtable% (%urltable%, %playertable%, %admintable%, %timetable%)" +
					" VALUES(\"%url%\", \"%player%\", \"%admin%\", \"%time%\")")
					.replace("%logtable%", cfg.getTableName())
					.replace("%url%", url)
					.replace("%player%", nick)
					.replace("%admin%", admin)
					.replace("%time%", time)
					.replace("%urltable%", cfg.getColumnURL())
					.replace("%playertable%", cfg.getColumnPlayer())
					.replace("%admintable%", cfg.getColumnAdmin())
					.replace("%timetable%", cfg.getColumnTime())
			).executeUpdate();
			Logger.log(3, "[DataBase.addToLog]", "DONE");
		} catch (Exception e) {
			Logger.log(1, "[DataBase.addToLog]", e.getMessage());
		}
	}


	public static ArrayList<String> getFromLogForNick(String nick) {
		Logger.log(3, "[DataBase.getFromLogForNick]", nick);
		ArrayList<String> tmp = new ArrayList<>();
		try {
			Connection c = connect();
			PreparedStatement ps = c.prepareStatement("select * from %table% where %player%=\"%nick%\""
					.replace("%table%", cfg.getTableName())
					.replace("%player%", cfg.getColumnPlayer())
					.replace("%nick%", nick)
			);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				tmp.add("%time% ; %admin% ; %url%"
						.replace("%time%", rs.getString(cfg.getColumnTime()))
						.replace("%admin%", rs.getString(cfg.getColumnAdmin()))
						.replace("%url%", rs.getString(cfg.getColumnURL()))
				);
			}
			rs.close();
		} catch (SQLException e) {
			Logger.log(1, "[DataBase.getFromLogForNick]", e.getMessage());
			e.printStackTrace();
		}
		if (!tmp.isEmpty())
			for (String tmpp : tmp)
				Logger.log(3, "[DataBase.getFromLogForNick]", tmpp);
		return tmp;
	}
}

