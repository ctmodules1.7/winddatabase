package ru.will0376.winddatabase;

import net.minecraftforge.common.config.Configuration;

import java.io.File;

public class Config {
	private final String DB = "DataBase";
	private final Configuration configuration;
	private boolean enabled;
	private String host;
	private String user;
	private String pass;
	private String tableName;
	private String columnURL;
	private String columnPlayer;
	private String columnAdmin;
	private String columnTime;

	public Config(File file) {
		this.configuration = new Configuration(file);
	}

	public void launch() {
		load();
		setConfigs();
		save();
	}

	private void load() {
		this.configuration.load();
	}

	private void save() {
		this.configuration.save();
	}

	private void setConfigs() {
		enabled = configuration.getBoolean("UseDataBase", DB, true, "Use the database?");
		host = configuration.getString("Host", DB, "jdbc:mysql://127.0.0.1:3306/test?serverTimezone=UTC", "ip:port/database?serverTimezone=UTC");
		user = configuration.getString("User", DB, "root", "User");
		pass = configuration.getString("Pass", DB, "pass", "Password");
		tableName = configuration.getString("Table", DB, "CT_Table_log", "Name of table =)");
		columnURL = configuration.getString("ColumnURL", DB, "URL", "column of URL");
		columnPlayer = configuration.getString("ColumnPlayer", DB, "Player", "column of Player");
		columnAdmin = configuration.getString("ColumnAdmin", DB, "Admin", "column of Admin");
		columnTime = configuration.getString("ColumnTime", DB, "Time", "column of Time");
	}

	public boolean isEnabled() {
		return enabled;
	}

	public String getHost() {
		return host;
	}

	public String getUser() {
		return user;
	}

	public String getPass() {
		return pass;
	}

	public String getTableName() {
		return tableName;
	}

	public String getColumnURL() {
		return columnURL;
	}

	public String getColumnPlayer() {
		return columnPlayer;
	}

	public String getColumnAdmin() {
		return columnAdmin;
	}

	public String getColumnTime() {
		return columnTime;
	}

}

