package ru.will0376.winddatabase;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import ru.will0376.windevents.events.EventPrintAnswer;
import ru.will0376.windevents.events.EventReloadConfig;
import ru.will0376.windevents.events.EventRequestPrintScreenLog;
import ru.will0376.windevents.utils.ChatForm;
import ru.will0376.windevents.utils.Logger;
import ru.will0376.windevents.utils.Utils;

import java.util.ArrayList;

public class Event {
	@SubscribeEvent
	public void catchScreen(EventPrintAnswer e) {
		if (WindDB.config.isEnabled()) {
			DataBase.addToLog(e.getPlayerNick(), e.getAdminNick(), e.getText(), System.currentTimeMillis() + "");
		}
	}

	@SubscribeEvent
	public void reload(EventReloadConfig e) {
		if (WindDB.config.isEnabled()) {
			WindDB.config.launch();
			if (Utils.getEPMP(e.getSender()) == null) Logger.log(1, "[Ctdatabase]", "Reloaded");
			else
				Utils.getEPMP(e.getSender()).addChatMessage(new ChatComponentText(EnumChatFormatting.GOLD + "[Ctdatabase] Reloaded"));
		}
	}

	@SubscribeEvent
	public void catchLogs(EventRequestPrintScreenLog e) {
		if (WindDB.config.isEnabled()) {
			ArrayList<String> list = DataBase.getFromLogForNick(e.getPlayer().getDisplayName());
			if (!list.isEmpty()) {
				ArrayList<String> tmplist = new ArrayList<>();
				list.stream().skip((e.getPage() - 1) * 5).forEach(a -> {
					if (list.indexOf(a) <= (e.getPage() - 1) * 5) tmplist.add(a);
				});
				if (e.getPlayer() == null) {
					tmplist.forEach(System.out::println);
				} else
					Utils.printWithCase(e.getPlayer(), ChatForm.prefix, tmplist);
			}
		}
	}
}

