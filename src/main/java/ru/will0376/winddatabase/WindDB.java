package ru.will0376.winddatabase;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import net.minecraft.util.EnumChatFormatting;
import net.minecraftforge.common.MinecraftForge;
import ru.will0376.windevents.events.EventLogPrint;
import ru.will0376.windevents.events.EventModStatusResponse;
import ru.will0376.windevents.utils.Logger;

@Mod(
		modid = WindDB.MOD_ID,
		name = WindDB.MOD_NAME,
		version = WindDB.VERSION,
		acceptableRemoteVersions = "*",
		dependencies = "required-after:windevents@[1.0,)"
)
public class WindDB {

	public static final String MOD_ID = "WindDB";
	public static final String MOD_NAME = "WindDB";
	public static final String VERSION = "1.0";
	public static Config config;
	@Mod.Instance(MOD_ID)
	public static WindDB INSTANCE;

	@Mod.EventHandler
	public void preinit(FMLPreInitializationEvent event) {
		config = new Config(event.getSuggestedConfigurationFile());
		config.launch();
		FMLCommonHandler.instance().bus().register(new Event());
	}

	@Mod.EventHandler
	public void init(FMLInitializationEvent event) {
		if (config.isEnabled()) {
			Logger.log(3, "[Main-DB-Starter]", "Checking DB");
			DataBase.checkTables();
		}
	}

	@Mod.EventHandler
	public void postinit(FMLPostInitializationEvent event) {
		MinecraftForge.EVENT_BUS.post(new EventLogPrint(3, EnumChatFormatting.GOLD + "[WindDB]" + EnumChatFormatting.RESET, "PostInit done!"));
		MinecraftForge.EVENT_BUS.post(new EventModStatusResponse(MOD_ID, config.isEnabled()));
	}
}

